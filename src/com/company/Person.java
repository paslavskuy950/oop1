package com.company;

public class Person {

    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void incrementAge() {
        age += 1;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //    public Person(String name, int age) {
//        this.name = name;
//        this.age = age;
//    }

    public void print() {
        System.out.println("Run!");
    }

    public void printValue(int a) {
        System.out.println(a);
    }

    public int add(int a, int b) {
        return a + b;
    }

    public void testMod() {
        System.out.println(name);
    }

}
